package ru.t1.shevyreva.tm.api.model;

import java.util.Date;

public interface IHaveCreated {

    Date getCreated();

    void setDate(Date created);

}
