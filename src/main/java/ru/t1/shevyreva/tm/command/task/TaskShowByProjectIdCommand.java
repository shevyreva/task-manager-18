package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Show tasks by project id.";

    private final String NAME = "task-show-by-project-id";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW ALL TASKS BY PROJECT ID]");
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getProjectTaskService().findAllTaskByProjectId(projectId);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
