package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.model.User;

public interface IAuthService {

    User registry(final String login, String password, String email);

    void login(String login, String password);

    void logout();

    User getUser();

    String getUserId();

}
