package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskCompleteStatusByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Complete task by Id.";

    private final String NAME = "task-update-status-by-id";


    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }

}
