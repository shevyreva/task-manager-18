package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.service.ILoggerService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public class LoggerService implements ILoggerService {

    private static final String CONFIG_FILE = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMANDS_FILE = "./commands.xml";

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = "./errors.xml";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = "./messages.xml";

    private static final LogManager MANAGER = LogManager.getLogManager();

    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    private static final Logger LOGGER_COMMANDS = Logger.getLogger(COMMANDS);

    private static final Logger LOGGER_ERRORS = Logger.getLogger(ERRORS);

    private static final Logger LOGGER_MESSAGES = Logger.getLogger(MESSAGES);

    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    static {
        loadConfig();
        registry(LOGGER_COMMANDS, COMMANDS_FILE, false);
        registry(LOGGER_ERRORS, ERRORS_FILE, false);
        registry(LOGGER_MESSAGES, MESSAGES_FILE, true);
    }

    public static Logger getLoggerCommands() {
        return LOGGER_COMMANDS;
    }

    public static Logger getLoggerErrors() {
        return LOGGER_ERRORS;
    }

    public static Logger getLoggerMessages() {
        return LOGGER_MESSAGES;
    }

    private static ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(
            final Logger logger,
            final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty()) {
                logger.addHandler(new FileHandler(fileName));
            }
        } catch (IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private static void loadConfig() {
        try {
            final Class<?> clazz = LoggerService.class;
            final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        LOGGER_ERRORS.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.info(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMANDS.info(message);
    }

}
