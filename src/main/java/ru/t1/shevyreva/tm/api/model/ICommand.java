package ru.t1.shevyreva.tm.api.model;

public interface ICommand {

    public String getName();

    public String getDescription();

    public String getArgument();

    void execute();

}
