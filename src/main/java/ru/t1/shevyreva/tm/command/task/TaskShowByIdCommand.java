package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    private final String NAME = "task-show-by-id";

    private final String DESCRIPTION = "Show task by Id.";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        showTask(task);
    }

}
