package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Create new task.";

    private final String NAME = "task-create";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

}
