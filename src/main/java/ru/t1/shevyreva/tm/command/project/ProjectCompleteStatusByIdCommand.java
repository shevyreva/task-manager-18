package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectCompleteStatusByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Complete project by Id.";

    private final String NAME = "project-update-status-by-id";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }
}
