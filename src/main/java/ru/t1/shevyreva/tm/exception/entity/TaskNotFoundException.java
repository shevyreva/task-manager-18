package ru.t1.shevyreva.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Task not found! ");
    }

}
