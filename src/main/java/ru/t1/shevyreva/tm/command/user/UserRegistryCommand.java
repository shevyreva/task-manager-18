package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.api.service.IAuthService;
import ru.t1.shevyreva.tm.model.User;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "User registration.";

    private final String NAME = "user-registry";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION]:");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = getAuthService();
        final User user = authService.registry(login, password, email);
        showUser(user);
    }

}
