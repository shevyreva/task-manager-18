# TASK MANAGER

## DEVELOPER INFO

**NAME:** Shevyreva Liya

**EMAIL:** liyavmax@gmail.com

**WORK EMAIL:** lmaximova@t1-consulting.ru

## SOFTWARE

**JAVA**: 1.8 OpenJDK

**OS**: Windows 10 Корпоративная LTSC

## HARDWARE

**CPU**: i5

**RAM**: 16GB

**SSD**: 237GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
