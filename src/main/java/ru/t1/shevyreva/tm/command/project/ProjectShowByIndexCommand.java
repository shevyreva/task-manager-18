package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    private final String NAME = "project-show-by-index";

    private final String DESCRIPTION = "Show project by Index.";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

}
