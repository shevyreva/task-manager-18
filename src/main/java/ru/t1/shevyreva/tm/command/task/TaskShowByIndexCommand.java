package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    private final String NAME = "task-show-by-index";

    private final String DESCRIPTION = "Show task by Index.";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findOneByIndex(index);
        showTask(task);
    }

}
