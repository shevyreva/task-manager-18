package ru.t1.shevyreva.tm.component;

import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.*;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.command.project.*;
import ru.t1.shevyreva.tm.command.system.*;
import ru.t1.shevyreva.tm.command.task.*;
import ru.t1.shevyreva.tm.command.user.*;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shevyreva.tm.exception.system.CommandNotSupportedException;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.CommandRepository;
import ru.t1.shevyreva.tm.repository.ProjectRepository;
import ru.t1.shevyreva.tm.repository.TaskRepository;
import ru.t1.shevyreva.tm.repository.UserRepository;
import ru.t1.shevyreva.tm.service.*;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ListCommand());
        registry(new ArgumentListCommand());
        registry(new ApplicationExitCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectsShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectStartStatusByIdCommand());
        registry(new ProjectStartStatusByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectCompleteStatusByIdCommand());
        registry(new ProjectCompleteStatusByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());

        registry(new TaskCreateCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TasksShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new TaskStartStatusByIdCommand());
        registry(new TaskStartStatusByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskCompleteStatusByIdCommand());
        registry(new TaskCompleteStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());

        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    public void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(final String[] args) {
        if (processArguments(args)) System.exit(0);

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println(e.getMessage());
                System.out.println("[FAILED]");
            }
        }
    }

    public boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    public void processArgument(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("**Welcome to Task Manager**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("**Shutdown Task Manager**");
            }
        });
    }

    private void initDemoData() {
        userService.create("JACK", "abc", "jack@gmail.com");
        userService.create("IVAN", "qqq", "ivan@yandex.ru");
        userService.create("ADMIN", "admin", Role.ADMIN);

        projectService.add(new Project("B PROJECT", Status.COMPLETED));
        projectService.add(new Project("A PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("D PROJECT", Status.NON_STARTED));
        projectService.add(new Project("C PROJECT", Status.IN_PROGRESS));

        taskService.add(new Task("MEGA TASK", Status.COMPLETED));
        taskService.add(new Task("SUPER TASK", Status.NON_STARTED));
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
