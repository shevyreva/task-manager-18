package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "User profile change.";

    private final String NAME = "user-update-profile";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter Last Name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter First Name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, middleName, lastName);
    }

}
