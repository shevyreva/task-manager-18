package ru.t1.shevyreva.tm.exception.field;

public class LoginEmptyException extends AbsrtactFieldException {

    public LoginEmptyException() {
        super("Error! Login is empty.");
    }

}
